/**
 * Created by jujha on 2018-06-18.
 */


var express = require('express')
var app = express()
var path = require('path');
var public = path.join(__dirname, 'public');

app.use(express.static('public'))
app.use(express.json());
app.use(express.urlencoded());

// respond with "hello world" when a GET request is made to the homepage
app.get('/', function (req, res) {
    res.send('Root,  nothing to see here folks')
})

app.get('/home', function (req, res) {
    res.sendFile(path.join(public, 'homepage.html'));
})

app.post('/post', function (req, res) {
    res.send(req.body.name + 'so des world' )
})

app.get('/post', function (req, res) {
    res.send('so world')
})


app.listen(3000, () => console.log('Example app listening on port 3000!'))